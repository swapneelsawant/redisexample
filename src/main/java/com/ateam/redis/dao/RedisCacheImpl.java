package com.ateam.redis.dao;

import com.ateam.redis.bo.Person;
import com.ateam.redis.config.ConfigManager;
import org.redisson.api.RMap;
import org.redisson.api.RSet;
import org.redisson.api.RedissonClient;

import java.util.*;
import java.util.function.Function;

import static java.util.Arrays.asList;

public class RedisCacheImpl<T> {

    private static final String PERSON_MAP = "PERSON_MAP";

    private static final String POSITION_MAP = "POSITION_MAP";

    private List<Function<Person, String>> INVERTED_INDEX_MAP = asList(this::getPositionIndexMapName);

    private RedissonClient redissonClient;

    public RedisCacheImpl() {
        redissonClient = ConfigManager.getInstance().getRedisson();
    }

    public void cache(Person person) {
        RMap<Long, Person> personMap = redissonClient.getMap(PERSON_MAP);
        personMap.put(person.getEmpId(), person);

        List<String> invertedIndexMapNames = getInvertedIndexMapName(person);
        for (String invertedIndexMapName: invertedIndexMapNames) {
            RSet<Long> invertedIndexMap = redissonClient.getSet(invertedIndexMapName);
            invertedIndexMap.add(person.getEmpId());
        }
    }

    public List<Person> get(Person person) {
        List<String> invertedIndexMapNames = getInvertedIndexMapName(person);
        if(invertedIndexMapNames.isEmpty())
            return Collections.emptyList();
        String invertedIndexMapName = invertedIndexMapNames.remove(0);
        RSet<Long> invertedIndexMap = redissonClient.getSet(invertedIndexMapName);
        Set<Long> personIDSet;
        if(invertedIndexMapNames.isEmpty()){
            personIDSet = invertedIndexMap.readAll();
        } else {
            personIDSet = invertedIndexMap.readIntersection(invertedIndexMapNames.toArray(new String[0]));
        }
        List<Person> persons = new ArrayList<>();
        RMap<Long, Person> personMap = redissonClient.getMap(PERSON_MAP);
        for (long employeeId: personIDSet) {
            persons.add(personMap.get(employeeId));
        }
        return persons;
    }

    private List<String> getInvertedIndexMapName(Person person) {
        List<String> invertedIndexMapNames = new ArrayList<>();
        for (Function<Person, String> inver: INVERTED_INDEX_MAP) {
            invertedIndexMapNames.add(inver.apply(person));
        }
        return invertedIndexMapNames;
    }

    private String getPositionIndexMapName(Person p) {
        return POSITION_MAP +"-"+ p.getPosition();
    }
}
