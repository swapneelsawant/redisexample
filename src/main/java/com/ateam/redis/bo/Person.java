package com.ateam.redis.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Person implements Serializable {

    private static final long serialVersionUID = 2798699543100552813L;

    private String employeeName;
    private long empId;
    private String position;
    private String state;
    private long zip;
    private String sex;
}
