package com.ateam.redis.config;

import org.redisson.Redisson;
import org.redisson.api.*;
import org.redisson.config.Config;

import java.io.IOException;

public final class ConfigManager {

    private static final ConfigManager CONFIG_MANAGER = new ConfigManager();

    public static ConfigManager getInstance() {
        return CONFIG_MANAGER;
    }

    private final RedissonClient redisson;

    private ConfigManager() {
        String cacheHostname = System.getenv("REDISCACHEHOSTNAME");
        String cachekey = System.getenv("REDISCACHEKEY");

        Config config = new Config();
        config.useSingleServer().setAddress(cacheHostname);
        config.useSingleServer().setPassword(cachekey);
        redisson = Redisson.create(config);
    }

    public RedissonClient getRedisson() {
        return redisson;
    }
}
