package com.ateam.redis.main;

import com.ateam.redis.bo.Person;
import com.ateam.redis.config.ConfigManager;
import com.ateam.redis.dao.RedisCacheImpl;

public class Main {

    public static void main(String[] args) {
        RedisCacheImpl  redisCache = new RedisCacheImpl();
        String employeeName = "Brown" ;
        long empId = 1103024456;
        String position = "Accountant";
        String state = "MA";
        long zip = 1450;
        String sex = "M";
        Person p = new Person(employeeName, empId, position, state, zip, sex);
        redisCache.cache(p);
        System.out.println(redisCache.get(p));
        ConfigManager.getInstance().getRedisson().shutdown();
    }
}
